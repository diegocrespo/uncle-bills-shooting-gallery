extends Node

var achievements = {
	"0": {
		"Achieved": false,
		"Name": "Green Horn",
		"Progress": 0,
		"Description": "Shoot 10 Targets"
	},
	"1": {
		"Achieved": false,
		"Name": "Young Cowboy",
		"Progress": 0,
		"Description": "Shoot 100 Targets"
	}
}

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
