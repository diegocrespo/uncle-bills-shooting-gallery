extends Node2D
var mousePos
onready var windowSize = get_viewport().size
var play_once = 0
signal fired
var can_shoot = true
export(int) onready var total_ammo
onready var ammo = total_ammo
signal reloaded
signal target_hit(points)

onready var timer = $Timer
export(float) var rof
var multiplier = 1
var hits = 0
var floatingPoints = preload("res://scenes/FloatingText.tscn")

func _ready():
	Input.set_custom_mouse_cursor(load("res://Art/crosshair_red_scaled.png"), Input.CURSOR_ARROW, Vector2(12.5,12.5))
	$Timer.wait_time = rof
	

func play_sound():
	var pitch = rand_range(.92, 1)
	$AudioStreamPlayer.set_pitch_scale(pitch)
	$AudioStreamPlayer.play()
	play_once = 1
	return $AudioStreamPlayer

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	mousePos = get_global_mouse_position()
	$Sprite.position.x = clamp(mousePos.x, 70, windowSize.x - (70 + $Sprite.offset.x))
	$Sprite.position.y = windowSize.y - $Sprite.texture.get_size().x - 75
	$Area2D.position.x = mousePos.x
	$Area2D.position.y = mousePos.y
	if ammo == 0:
		block_cursor()
	if Input.is_action_just_pressed("ui_click") and can_shoot == true and ammo > 0:
		shoot()
		muzzle_flash()
		call_shake()
	if Input.is_action_just_pressed("reload"):
		reload()
		

func _on_SpawnNode_target_hit():
	$AnimationPlayer.play("Recoil")
	
func shoot():
	check_overlap()
	if can_shoot and ammo > 0:
		can_shoot = false
		$Timer.start()
		$AnimationPlayer.play("Recoil")
		play_sound()
		emit_signal("fired")
		ammo -= 1
	elif ammo == 0:
		$AudioStreamPlayer3.play()

class MyCustomSorter:
	static func num_sort(a,b):
		if a > b:
			return true
		else:
			return false
			
func reload():
	emit_signal("reloaded")
	ammo = total_ammo
	$AudioStreamPlayer2.play()
	$CursorBlock.hide()
	$Label.hide()
	
func _on_Timer_timeout():
	can_shoot = true
	
func block_cursor():
	"""Spawns a rectangle with no alpha to intercept clicks"""
	if !($CursorBlock.visible) and !($Label.visible):
		$CursorBlock.show()
		$Label.show()
	$CursorBlock.rect_position = mousePos
	$CursorBlock.rect_position.x = $CursorBlock.rect_position.x - 50
	$CursorBlock.rect_position.y = $CursorBlock.rect_position.y - 50
	$Label.rect_position = mousePos
	$Label.rect_position.x = $Label.rect_position.x - 50
	$Label.rect_position.y = $Label.rect_position.y - 20
	
func check_overlap():
	"""Used to check the overlaps of the area2d over the cursor
	   to determine what function to use for each node"""
	var objs_dict = {}
	var priority_list = []
	var overlapping = $Area2D.get_overlapping_areas()
	#use inner ring to determine points calc
	var inner_ring = false
	if overlapping:
		# Iterate through the overlapping areas
		for node in overlapping:
			if node.get_name() == "InnerRing":
				inner_ring = true
			objs_dict[node.owner.priority] = node.owner
			priority_list.append(node.owner.priority)
		if priority_list:
			# if there is only one overlapping area
			if priority_list[0] < 0 and len(priority_list) > 1:
				# sort didn't work but a custom sort did
				priority_list.sort_custom(MyCustomSorter,"num_sort")
			# Get the object with the highest priority
			var active_obj = objs_dict[priority_list[0]]
			var split_numbers = split_number(active_obj.get_name())
			print("HIT %s" % split_numbers)
			match split_numbers:
				#I'm lazy to figure out why it's not splitting
				"@Letter@":
					# Hud 
					var parent = active_obj.get_parent()
					show_hit(active_obj.points * multiplier, active_obj)
					parent.add_combo(active_obj)
				"Letter":
					# Hud 
					var parent = active_obj.get_parent()
					show_hit(active_obj.points * multiplier, active_obj)
					parent.add_combo(active_obj)
				"Tree":
					active_obj.play_animation()
					miss()
				"BackPanel":
					active_obj.place_decal()
					miss()
				"Target":
					active_obj.tear_down()
					inc_hits()
					if inner_ring == true:
						show_hit(active_obj.points * 2 * multiplier, active_obj)
					else:
						show_hit(active_obj.points * multiplier, active_obj)
				"TargetSprite":
					print("I HIT")
					var spawnNode = active_obj.get_parent().get_parent()
					spawnNode.target_hit()
					inc_hits()
					if inner_ring == true:
						show_hit(active_obj.points * 2 * multiplier, active_obj)
					else:
						show_hit(active_obj.points * multiplier, active_obj)
				"Duck":
					var spawnNode = active_obj.get_parent().get_parent()
					# to get spawnNode need to go up one more lvl for path2d
					if spawnNode.get_name() == "MovingTargetPath":
						spawnNode = spawnNode.get_parent()
					spawnNode.target_hit()
					inc_hits()
					if inner_ring == true:
						print("HIT INNER RING")
						show_hit(active_obj.points * 2 * multiplier, active_obj)
					else:
						print("TARGET_SPRITE_HIT")
						show_hit(active_obj.points * multiplier, active_obj)
	inner_ring = false

func split_number(word):
	"""Returns a string without the trailing numbers"""
	# string to int makes 0
	if int(word[-2]) != 0:
		return word.substr(0, len(word) -2)
	elif int(word[-1]) != 0:
		return word.substr(0, len(word) -1)
	else:
		return word
		
func show_points(targetPoints):
	mousePos = get_global_mouse_position()
	var floatText = floatingPoints.instance()
	floatText.position.x = mousePos.x
	floatText.position.y = mousePos.y
	floatText.velocity = Vector2(rand_range(-50, 50), -100)
	floatText.modulate = Color(rand_range(0.7, 1), rand_range(0.7, 1), rand_range(0.7, 1), 1.0)
	floatText.text = "+ %s" % targetPoints
	get_parent().add_child(floatText)

func inc_hits():
	"""Handles multiplier and displaying multiplier text"""
	hits += 1
	if hits % 10 == 0:
		multiplier *=2
		if get_parent().get_node("Hud"):
			var multLabel = get_parent().get_node("Hud/Label")
			if multiplier == 2:
				multLabel.level_one(multiplier)
			elif multiplier > 2 and multiplier <= 4:
				multLabel.level_two(multiplier)
			else:
				multLabel.level_three(multiplier)
func show_hit(pnts,obj):
	emit_signal("target_hit",pnts)
	obj.place_decal()
	show_points(pnts)

func miss():
	hits = 0
	multiplier = 1
	if get_parent().get_node("Hud"):
		var multLabel = get_parent().get_node("Hud/Label")
		multLabel.level_zero()

func muzzle_flash():
	$Sprite/AnimatedSprite.frame = 0
	$Sprite/AnimatedSprite.show()
	$Sprite/AnimatedSprite.play("Muzzle Flash")
	yield($Sprite/AnimatedSprite, "animation_finished")
	$Sprite/AnimatedSprite.hide() 

func call_shake():
	get_parent().get_node("Camera2D/ScreenShake").screen_shake(.1,20, 100)
