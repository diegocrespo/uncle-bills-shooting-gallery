extends Position2D

var time_out = 1 # Time before spawning another Target
var wait_time = 0 # Keep track of time passed so Target can spawn correctly
export(int) var Respawns # Number of times a target will respawn before no more
export(int) var target_rotation # starting orientation
export(int) var target_end_rotation
export(float) var life_time
# if anim hasn't finished before respawn the target will animate crooked
export(float) var respawn_time 
export(float) onready var delay
export(PackedScene) onready var Target
export(PackedScene) onready var child_subscene
var hit_count = 0
var final_count = 0
var target_points = 0
var last_life = false
onready var delayTimer = get_node("DelayTime")
onready var TargetInstance = Target.instance()
signal miss()


func _ready():
	$Respawn.wait_time = respawn_time
	$LifeTime.wait_time = life_time
	$DelayTime.wait_time = delay
	target_points = TargetInstance.points
	if child_subscene:
		TargetInstance.Target = child_subscene
	if TargetInstance.get_name() == "Stick":
		TargetInstance.rotation_degrees = target_rotation
	self.add_child(TargetInstance)
	if delay < .2:
		print("DELAY IS ZERO")
		Respawns -= 1
		TargetInstance.setup(target_end_rotation)
		$LifeTime.start()
	elif delay > 0:
		delayTimer.start()


func spawnTarget():
	Respawns -= 1
	TargetInstance.anim_rotate(target_end_rotation)
	$LifeTime.start()


func _on_DelayTime_timeout():
	"""setup param does nothing for base target"""
	print("DELAY TIMEOUT")
	TargetInstance.setup(target_end_rotation)
	Respawns -= 1
	$LifeTime.start()


func _on_LifeTime_timeout():
	TargetInstance.teardown(-target_rotation)
	emit_signal("miss")
	if Respawns > 0:
		$Respawn.start()
	else:
		pass


func _on_Respawn_timeout():
	print("RESPAWNINGGGG")
	if Respawns > 0:
		Respawns -= 1
		TargetInstance.setup(target_rotation)
		$LifeTime.start()
		if Respawns == 0:
			last_life = true
		else:
			pass


func target_hit():
	$LifeTime.stop()
	$Respawn.start()
	TargetInstance.teardown(target_end_rotation)
