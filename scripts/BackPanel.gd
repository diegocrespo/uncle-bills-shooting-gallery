extends Node2D

var decal: String = "res://Art/shot_grey_large.png"
export(int) var priority = -100
# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Play sound randomly pitch shifted 
func play_sound(): 
	var pitch = rand_range(.35, .4)
	$AudioStreamPlayer.set_pitch_scale(pitch)
	$AudioStreamPlayer.play()
	return $AudioStreamPlayer 

func place_decal() -> void:
	var decalInstance = Sprite.new()
	decalInstance.texture = load(decal)
	decalInstance.position = self.get_local_mouse_position()
	add_child(decalInstance)
	yield(play_sound(), "finished")

