extends Node2D

export(int) var priority
export(Texture) var grass_texture = load("res://Art/grass1.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.texture = grass_texture


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
