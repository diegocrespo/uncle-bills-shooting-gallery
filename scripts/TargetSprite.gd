extends Sprite

var play_once: int = 0
export(int) var points: int = 1
export(int) var priority: int
export(int) var health: int = 1
export(Texture) var decal_texture: Texture
signal hit(points)

func play_sound():
	""" Play sound randomly pitch shifted """
	var pitch: float = rand_range(.92, 1.0)
	$AudioStreamPlayer.set_pitch_scale(pitch)
	$AudioStreamPlayer.play()
	play_once = 1
	return $AudioStreamPlayer
	
func place_decal() -> void:
	var decal_instance: Sprite = Sprite.new()
	decal_instance.texture = decal_texture
	decal_instance.position = get_local_mouse_position()
	add_child(decal_instance)
	yield(play_sound(), "finished")
	emit_signal("hit",points)

func setup(param):
	"""setup will vary based on inherited node. If it's a TargetSprite it will
	   do nothing. It takes a param because some of the later functions need
	   at least one parameter. That way I can ensure that the targets who need
	   the param do something with it, and the targets that don't, don't fail
	   when they are spawned and the method is called with a param"""
	enable_collisions()
	return param
	
func tear_down():
	"""Similar to setup"""
	disable_collisions()


#func _on_Area2D_input_event(_viewport, event, _shape_idx):
#	if event is InputEventMouseButton:
#		if Input.is_action_pressed("ui_click"):
#			place_decal()
			
func disable_collisions():
	$OuterRing/CollisionShape2D.disabled = true
	$InnerRing/CollisionShape2D.disabled = true
	
func enable_collisions():
	$OuterRing/CollisionShape2D.disabled = false
	$InnerRing/CollisionShape2D.disabled = false

func _on_OuterRing_input_event(_viewport, _event, _shape_idx):
	pass # Replace with function body.


func _on_InnerRing_input_event(_viewport, _event, _shape_idx):
	pass # Replace with function body.
