extends Node2D

signal times_up
export(int) var lifetime
onready var timer = get_node("CanvasLayer/TextureRect/Timer")
# Called when the node enters the scene tree for the first time.
func _ready():
	timer.set_wait_time(lifetime)
	timer.start()

func _on_Timer_timeout():
	$CanvasLayer/TextureRect.show()
	emit_signal("times_up")
