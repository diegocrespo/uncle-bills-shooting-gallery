extends Node2D

var play_once = 0
export(int) var points = 1
export(int) var priority
onready var TweenNode = $Tween
var back = true
export(int) var health = 1
signal hit(points)
var decal = "res://Art/shot_grey_large.png"
func _ready():
	pass

# Play sound randomly pitch shifted 
func play_sound():
	var pitch = rand_range(.92, 1)
	$AudioStreamPlayer.set_pitch_scale(pitch)
	$AudioStreamPlayer.play()
	play_once = 1
	return $AudioStreamPlayer

func _on_Area2D_input_event(_viewport, event, _shape_idx):
	if event is InputEventMouseButton:
		if Input.is_action_pressed("ui_click"):
			var decalInstance = Sprite.new()
			decalInstance.texture = load(decal)
			decalInstance.position = $"Stick".get_local_mouse_position()
			$Stick.add_child(decalInstance)
			yield(play_sound(), "finished")
			emit_signal("hit",points)
			
	else:
		pass
		
func anim_rotate(rotation):
		TweenNode.interpolate_property($Stick, "rotation_degrees", $Stick.rotation_degrees, $Stick.rotation_degrees + rotation, 1.33, Tween.TRANS_BACK, Tween.EASE_OUT)
		TweenNode.start()


