extends Sprite


onready var path_follow = get_parent()

var speed = 500
var move_direction = 0
var decal = "res://Art/shot_grey_large.png"
var play_once = 0
signal hit(points)
var points = 10
# Play sound randomly pitch shifted 
func play_sound():
	var pitch = rand_range(.92, 1)
	$AudioStreamPlayer.set_pitch_scale(pitch)
	$AudioStreamPlayer.play()
	play_once = 1
	return $AudioStreamPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func movement_loop(delta):
	var prepos = path_follow.get_global_position()
	path_follow.set_offset(path_follow.get_offset() + speed * delta)
	var pos = path_follow.get_global_position()
	move_direction = (pos.angle_to_point(prepos) / 3.14) *180
func _physics_process(delta):
	movement_loop(delta)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if Input.is_action_pressed("ui_click"):
			var decalInstance = Sprite.new()
			decalInstance.texture = load(decal)
			decalInstance.position = self.get_local_mouse_position()
			self.add_child(decalInstance)
			yield(play_sound(), "finished")
			emit_signal("hit",points)
