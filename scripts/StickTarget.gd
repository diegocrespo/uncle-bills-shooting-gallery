extends Sprite


# Make points accessible via the root node for SpawnNode
onready var points
export(PackedScene) onready var Target
var TargetInstance
var target_size
# Called when the node enters the scene tree for the first time.
func _ready():
	TargetInstance = Target.instance()
	points = TargetInstance.points
	target_size = TargetInstance.texture.get_size()
	match TargetInstance.get_name():
		"Duck":
			TargetInstance.position.y = -target_size.y - 50
		"TargetSprite":
			TargetInstance.position.y = -target_size.y - 39
	add_child(TargetInstance)

func setup(deg):
	TargetInstance.enable_collisions()
	anim_rotate(deg)
	
func teardown(deg):
	TargetInstance.disable_collisions()
	anim_rotate(deg)

func anim_rotate(rotation):
	$Tween.interpolate_property(self, "rotation_degrees", self.rotation_degrees, self.rotation_degrees + rotation, 1.33, Tween.TRANS_BACK, Tween.EASE_OUT)
	$Tween.start()
