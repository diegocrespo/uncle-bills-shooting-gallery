extends Node2D
var achievements = {}

# Changes achievement value to unlocked. Good for no tracking based achievements
func unlocked(id):
	achievements[id]["Achieved"] = true
# Updates achievements with counters that are being tracked
func update_achievement(id,field,value):
	achievements[id][field] = value
	print(achievements)
	var file = File.new()
	file.open("res://Achievements.json", file.WRITE)
	file.store_string(to_json(achievements))
	file.close()

# Called when the node enters the scene tree for the first time.
func _ready():
	var file = File.new()
	file.open("res://Achievements.json", file.READ)
	achievements = JSON.parse(file.get_as_text()).result
	file.close()
	#update_achievement("0","Achieved",true)
	
#	file.open("res://Achievements.json", file.READ)
#	achievements = JSON.parse(file.get_as_text()).result
#	file.close()
#	update_achievement("0","Achieved",true)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
