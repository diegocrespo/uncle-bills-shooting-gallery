extends RichTextLabel

export(int) var speed = 200
signal clicked()
export(int) var priority
export(int) var points
var letter
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
# satisfies showpoints
func place_decal():
	pass
func _process(delta):
	self.rect_position.y += speed * delta

func drop(tex):
	randomize()
	self.rect_position.y = 0
	self.bbcode_text = "[tornado radius=5 freq=5][rainbow]%s[rainbow]" % tex
	self.rect_position.x = rand_range(300, 900)

func _input(event):
	if event is InputEventMouseButton:
		emit_signal("clicked")
