extends Control

var new_score = 0
var new_score2 = 0
var new_score3 = 0
var new_score4 = 0
var digits = ""
var score = 0
var bulletIcon = "res://Art/icon_bullet_silver_short.png"
var shellIcon = "res://Art/icon_bullet_empty_short.png"
var letter = load("res://scenes/Letter.tscn")
var c_letter = false
var o_letter = false
var m_letter = false
var b_letter = false
var o2_letter = false
onready var bullet_container = get_parent().get_node("Hud/HBoxContainer2")
export(String) var Stage
var hits = 0
var miss = 0
var elapsed_time = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	set_score(score)
	$ComboTimer.start()
	if get_parent().get_node("Rifle"):
		set_bullets(get_parent().get_node("Rifle").total_ammo)
	else:
		set_bullets(6)
func _process(_delta):
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
	elapsed_time = OS.get_ticks_msec() / 1000.0
	if elapsed_time > 10:
		$CenterContainer/Label.text = str(elapsed_time).substr(0,4)
	else:
		$CenterContainer/Label.text = str(elapsed_time).substr(0,3)


func set_score(points):

	score += points
	if score < 10:
		new_score = load("res://Art/text_" + str(score) + "_small.png")
		$HBoxContainer/FirstDigitMargin/ScoreNumber.set_texture(new_score)
		
	if score > 9 and score < 99:
		if not $HBoxContainer/SecondDigitMargin/ScoreNumber.is_visible():
			$HBoxContainer/SecondDigitMargin/ScoreNumber.show()
		digits = str(score)
		new_score = load("res://Art/text_" + digits[0] + "_small.png")
		new_score2 = load("res://Art/text_" + digits[1] + "_small.png")
		$HBoxContainer/FirstDigitMargin/ScoreNumber.set_texture(new_score)
		$HBoxContainer/SecondDigitMargin/ScoreNumber.set_texture(new_score2)

	if score > 99 and score < 999:
		if not $HBoxContainer/ThirdDigitMargin/ScoreNumber.is_visible():
			$HBoxContainer/ThirdDigitMargin/ScoreNumber.show()
		# Scenario when 3 digit score on first hit and 2nd number never unhidden
		if not $HBoxContainer/SecondDigitMargin/ScoreNumber.is_visible():
			$HBoxContainer/SecondDigitMargin/ScoreNumber.show()
		digits = str(score)
		new_score = load("res://Art/text_" + digits[0] + "_small.png")
		new_score2 = load("res://Art/text_" + digits[1] + "_small.png")
		new_score3 = load("res://Art/text_" + digits[2] + "_small.png")
		$HBoxContainer/FirstDigitMargin/ScoreNumber.set_texture(new_score)
		$HBoxContainer/SecondDigitMargin/ScoreNumber.set_texture(new_score2)
		$HBoxContainer/ThirdDigitMargin/ScoreNumber.set_texture(new_score3)

	if score > 999 and score < 9999:
		if not $HBoxContainer/FourthDigitMargin/ScoreNumber.is_visible():
			$HBoxContainer/FourthDigitMargin/ScoreNumber.show()
		if not $HBoxContainer/ThirdDigitMargin/ScoreNumber.is_visible():
			$HBoxContainer/ThirdDigitMargin/ScoreNumber.show()
		# Scenario when 3 digit score on first hit and 2nd number never unhidden
		if not $HBoxContainer/SecondDigitMargin/ScoreNumber.is_visible():
			$HBoxContainer/SecondDigitMargin/ScoreNumber.show()
		digits = str(score)
		new_score = load("res://Art/text_" + digits[0] + "_small.png")
		new_score2 = load("res://Art/text_" + digits[1] + "_small.png")
		new_score3 = load("res://Art/text_" + digits[2] + "_small.png")
		new_score4 = load("res://Art/text_" + digits[3] + "_small.png")
		$HBoxContainer/FirstDigitMargin/ScoreNumber.set_texture(new_score)
		$HBoxContainer/SecondDigitMargin/ScoreNumber.set_texture(new_score2)
		$HBoxContainer/ThirdDigitMargin/ScoreNumber.set_texture(new_score3)
		$HBoxContainer/FourthDigitMargin/ScoreNumber.set_texture(new_score4)

func expend_ammo():
	# Swaps the bullet texture to a shell every time it's called
	var bullet
	var bulletTexture
	for container in $HBoxContainer2.get_children():
		bullet = container.get_children()[0]
		bulletTexture = bullet.get_texture().get_path()
		if bulletTexture == bulletIcon:
			bullet.set_texture(load(shellIcon))
			break
		else:
			pass
			
			
func reload():
	# Swaps the bullet texture to a bullet icon if it is a shell
	var bullet
	var bulletTexture
	for container in $HBoxContainer2.get_children():
		bullet = container.get_children()[0]
		bulletTexture = bullet.get_texture().get_path() # name of the texture
		if bulletTexture == shellIcon:
			bullet.set_texture(load(bulletIcon))


func _on_Rifle_fired():
	expend_ammo()


func _on_Rifle_reloaded():
	reload()


func _on_SpawnNode_miss():
	hits = 0
	
func _on_Rifle_target_hit(points):
	set_score(points)


func set_bullets(number):
	for _num in range(number):
		var margin = MarginContainer.new()
		var bullet_tex = TextureRect.new()
		bullet_tex.texture  = load(bulletIcon)
		margin.add_child(bullet_tex)
		bullet_container.add_child(margin)
	bullet_container.rect_size.x = 25 * number
	# 1366 is current project width
	bullet_container.rect_position.x = 1366 - bullet_container.rect_size.x

func _on_Rifle_rifle_ready(num_bullets):
	set_bullets(num_bullets)


func _on_ComboTimer_timeout():
	# Figure out how to check the letter so you can add the proper combo
	if c_letter == false:
		var new_letter = letter.instance()
		new_letter.get_node("VisibilityNotifier2D").connect(
			"screen_exited", self, "_on_VisibilityNotifier2D_screen_exited")
		new_letter.letter = "C"
		add_child(new_letter)
		new_letter.drop("C")
		c_letter = true
	elif o_letter == false:
		var new_letter = letter.instance()
		new_letter.get_node("VisibilityNotifier2D").connect(
			"screen_exited", self, "_on_VisibilityNotifier2D_screen_exited")
		new_letter.letter = "O"
		add_child(new_letter)
		new_letter.drop("O")
		o_letter = true
	elif m_letter == false:
		var new_letter = letter.instance()
		new_letter.get_node("VisibilityNotifier2D").connect(
			"screen_exited", self, "_on_VisibilityNotifier2D_screen_exited")
		new_letter.letter = "M"
		add_child(new_letter)
		new_letter.drop("M")
		m_letter = true
	elif b_letter == false:
		var new_letter = letter.instance()
		new_letter.get_node("VisibilityNotifier2D").connect(
			"screen_exited", self, "_on_VisibilityNotifier2D_screen_exited")
		new_letter.letter = "B"
		add_child(new_letter)
		new_letter.drop("B")
		b_letter = true
	elif o2_letter == false:
		var new_letter = letter.instance()
		new_letter.get_node("VisibilityNotifier2D").connect(
			"screen_exited", self, "_on_VisibilityNotifier2D_screen_exited")
		new_letter.letter = "O"
		add_child(new_letter)
		new_letter.drop("O")
		o2_letter = true


func add_combo(obj):
	$ComboText.bbcode_text += obj.letter
	obj.queue_free()


func _on_VisibilityNotifier2D_screen_exited():
	""" If letter goes off screen then add blank space to combo text"""
	print("THE TEXT IS GONE")
	$ComboText.bbcode_text += "  "
