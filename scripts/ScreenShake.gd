extends Node2D
var current_shake_priority = 0

func _ready():
	pass
func move_camera(vector):
	get_parent().offset = Vector2(rand_range(-vector.x, vector.x), rand_range(-vector.y, vector.y))
	
func screen_shake(shake_length, shake_power, shake_priority):
	if shake_priority > current_shake_priority:
		current_shake_priority = shake_priority
		$Tween.interpolate_method(self, "move_camera", Vector2(shake_power, shake_power), Vector2(0,0), shake_length, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
		$Tween.start()
		
#var amplitude = 0
#const TRANS = Tween.TRANS_SINE
#const EASE = Tween.EASE_IN_OUT
#onready var camera = get_parent()
#
#func start(duration = 0.2, frequency = 15, amplitude = 16):
#	self.amplitude = amplitude
#	$Timer2.wait_time = duration
#	$Timer.wait_time = 1 / float(frequency)
#	$Timer2.start()
#	$Timer.start()
#
#func _new_shake():
#	var rand = Vector2()
#	rand.x = rand_range(-amplitude, amplitude)
#	rand.y  = rand_range(-amplitude, amplitude)
#
#	$Tween.interpolate_property(camera, "offset",camera.offset, rand, $Timer.wait_time, TRANS, EASE)
#	$Tween.start()
#
#func _on_Timer_timeout():
#	_new_shake()
#
#func _reset():
#	$Tween.interpolate_property(camera, "offset",camera.offset, Vector2(), $Timer.wait_time, TRANS, EASE)
#	$Tween.start()
#
#func _on_Timer2_timeout():
#	_reset()
#	$Timer2.stop()
#
#func _process(delta):
#	if Input.is_action_pressed("ui_accept"):
#		start()


func _on_Tween_tween_completed(_object, _key):
	current_shake_priority = 0
