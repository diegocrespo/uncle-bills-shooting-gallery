extends Path2D

onready var path_follow = $PathFollow2D
export(int) onready var speed
export(PackedScene) onready var target_scene
export(bool) var flip_img_h
export(bool) var flip_img_v
var points
var targetInstance


func _ready():
	print("MOVING PATH", self)
	targetInstance = target_scene.instance()
	points = targetInstance.points
	if flip_img_h == true:
		targetInstance.scale.x = targetInstance.scale.x * -1
	if flip_img_v == true:
		targetInstance.scale.y = targetInstance.scale.y * -1
	path_follow.add_child(targetInstance)
	
func setup(param):
	targetInstance.enable_collisions()
	path_follow.unit_offset = 0
	targetInstance.show()
	return param


func teardown(param):
	targetInstance.disable_collisions()
	targetInstance.hide()
	return param
		
func _process(delta):
	if Input.is_action_just_pressed("ui_select"):
		setup("foo")
	if path_follow.unit_offset >= .99:
		teardown("foo")
	else:
		path_follow.set_offset(path_follow.get_offset() + speed * delta)
