extends Node2D
var score = 0


func play_animation(AnimPlay):
	AnimPlay.play("Start")
	return AnimPlay
	


func _ready():
	get_tree().paused = true
	$BackgroundMusic.play()
	yield(play_animation($TransitionScene/AnimationPlayer), "animation_finished")
	get_tree().paused = false
	$TransitionScene.hide()
	$Rifle.show()
	connect_signals()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func connect_signals():
	print("SIGNALS")
	print($Rifle.connect("fired", $Hud, "_on_Rifle_fired"))
	print($Rifle.connect("reloaded", $Hud, "_on_Rifle_reloaded"))
	print($Rifle.connect("target_hit", $Hud, "_on_Rifle_target_hit"))
