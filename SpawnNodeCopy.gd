extends Position2D
var targetscene = load("res://Target.tscn")
var timeOut = 1 # Time before spawning another Target
var waitTime = 0 # Keep track of time passed so Target can spawn correctly
export(int) var Respawns # Number of times a target will respawn before no more
export(int) var targetRotation # starting orientation
export(int) var targetEndRotation
export(int) var lifeTime
export(int) var Delay
signal target_hit(points,multiplier)
var hitCount = 0
var finalCount = 0
var targetPoints = 0
var lastLife = false
var floatingPoints = preload("res://FloatingText.tscn")
var targetPos
onready var delayTimer = get_node("DelayTime")
signal miss()

# Called when the node enters the scene tree for the first time.
func _ready():
	if Delay == 0:
		spawnTarget(targetscene)
	elif Delay > 0:
		delayTimer.wait_time = Delay
		delayTimer.start()

# Spawn target, animates, and starts it's lifetime
func spawnTarget(targetNode):
	for child in self.get_children():
		if child.get_name() == "Target":
			child.queue_free()
	var target = targetNode.instance()
	target.rotation_degrees = targetRotation
	targetPoints = target.points
	add_child(target)
	targetPos = $Target.position

	for child in self.get_children():
		if child.get_name() == "Target":
			child.anim_rotate(targetEndRotation)
			$LifeTime.start()
			break


func _process(_delta):
	# Check to see if there is a target)
	var children = self.get_children()
	var childNames = []
	for child in children:
		childNames.append(child.get_name())
	if !("Target" in childNames) and Respawns > 0 and $LifeTime.wait_time > 0:
		if $Respawn.time_left > 0:
			pass
		# waiting to start so don't spawn
		elif !($DelayTime.is_stopped()):
			pass
		else:
			show_points()
			emit_signal("target_hit", targetPoints)
			$Respawn.start()
	# Ensure final life gets scored if it's hit
	if !("Target" in childNames) and Respawns == 0 and $LifeTime.wait_time > 0 and lastLife == true:
		show_points()
		emit_signal("target_hit", targetPoints)
		lastLife = false

# Respawn the target
func _on_Respawn_timeout():
	if Respawns > 0:
		spawnTarget(targetscene)
		Respawns -= 1
		$LifeTime.start()
		if Respawns == 0:
			lastLife = true
	else:
		pass

# If the Target isn't shot fast enough make it disappear
func _on_LifeTime_timeout():
	for child in self.get_children():
		if child.get_name() == "Target":
			var tween = child.get_children()[-1].get_path()
			child.anim_rotate(targetEndRotation * -1)
			yield(get_node(tween), "tween_completed")
			child.queue_free()
			$Respawn.start()
			emit_signal("miss")
			break
			
func show_points():
	var floatText = floatingPoints.instance()
	floatText.position = targetPos
	floatText.position.y = - 200
	floatText.velocity = Vector2(rand_range(-50, 50), -100)
	floatText.modulate = Color(rand_range(0.7, 1), rand_range(0.7, 1), rand_range(0.7, 1), 1.0)
	floatText.text = "+ %s" % targetPoints
	add_child(floatText)


func _on_DelayTime_timeout():
	spawnTarget(targetscene)
