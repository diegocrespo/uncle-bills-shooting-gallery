extends RichTextLabel


var level = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func level_one(mult):
	show()
	self.bbcode_text = "[shake rate=100 level=10][color=#e62a03]X %s MULTIPLIER[/color] [/shake]" % mult
	level = 1


func level_two(mult):
	show()
	self.bbcode_text = "[shake rate=100 level=20][color=#e62a03]X %s MULTIPLIER[/color] [/shake]" % mult
	level = 2


func level_three(mult):
	show()
	self.bbcode_text = "[shake rate=100 level=30][color=#e62a03]X %s MULTIPLIER[/color] [/shake]" % mult
	level = 3


func level_zero():
	self.bbcode_text = ""
	level = 0
	hide()
