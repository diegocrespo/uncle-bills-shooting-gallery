extends Node2D
var score = 0
func _ready():
	Input.set_custom_mouse_cursor(load("res://Art/crosshair_red_large.png"), Input.CURSOR_ARROW, Vector2(25,25))
	$Targets/Target/AnimationPlayer.play("Raise Up")
	$Targets/Target2/AnimationPlayer.play("Rotate CC180")


func _on_Target_target_hit():
	# probably have to pass target to the score function at some point
	score += 1
	$Hud.set_score(score)
	
func _on_TimeUp_times_up():
	print("YES")
