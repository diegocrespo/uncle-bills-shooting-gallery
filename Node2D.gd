extends Node2D

var back = true
export(int) var priority = 1
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	
func play_sound():
	var pitch = rand_range(.92, 1)
	$AudioStreamPlayer.set_pitch_scale(pitch)
	$AudioStreamPlayer.play()
	return $AudioStreamPlayer
	
func play_animation():
	yield(play_sound(), "finished")
	$AnimationPlayer.play("Wiggle")

#	if event is InputEventMouseButton:
#		if Input.is_action_pressed("ui_click"):
#			yield(play_sound(), "finished")
#			$AnimationPlayer.play("Wiggle")


